package com.ness.request;

import java.util.List;

public class OrderRequest {

	private List<Integer>  itemIds;
	
	private int userId;

	

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public List<Integer> getItemIds() {
		return itemIds;
	}

	public void setItemIds(List<Integer> itemIds) {
		this.itemIds = itemIds;
	}
}
