package com.ness.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ness.entity.Item;
import com.ness.service.ItemService;

@RestController
@RequestMapping("/v1/")
@CrossOrigin("*")
public class ItemController {

	
	@Autowired
	ItemService itemService;
	
	
	
	@PostMapping("/items")
	public ResponseEntity<List<Item>> createItems(@RequestBody List<Item> items)
	{
		
		List<Item> result = itemService.createItems(items);
		return ResponseEntity.status(201).body(result);
	}
	
	
	@GetMapping("/items")
	public ResponseEntity<List<Item>> getItems(@RequestParam(name = "foodType",required = false) String foodType )
	{
		if(foodType!=null && !foodType.equals(""))
		{
			return ResponseEntity.status(200).body(itemService.getItems(foodType));
		}
		else
		{
			return ResponseEntity.status(200).body(itemService.getItems());
		}
	}
}
