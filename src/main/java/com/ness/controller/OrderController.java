package com.ness.controller;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ness.entity.Item;
import com.ness.entity.Order;
import com.ness.repository.ItemRepository;
import com.ness.repository.OrderRepositroy;
import com.ness.request.OrderRequest;
import com.ness.service.ItemService;

@RestController
@CrossOrigin("*")
public class OrderController {

	@Autowired
	ItemService itemService;

	@Autowired
	OrderRepositroy orderRepository;
	
	@Autowired
	ItemRepository itemRepository;

	@PostMapping("/v1/orders")
	public ResponseEntity<Map<String, String>> createOrder(@RequestBody OrderRequest request) {
		Map<String, String> responseMap = new HashMap<>();
		List<Integer> ids = request.getItemIds();
		List<Item> items = itemService.findItems(ids);
		Map<Integer, Item> itemMap = items.stream().collect(Collectors.toMap(Item::getId, Function.identity()));
		for (Item tempItem : items) {
			Integer itemId = tempItem.getId();
			Item itemFromDb = itemMap.get(itemId);
			if (itemFromDb.getQuanitity() < 1) {
				responseMap.put(itemFromDb.getName(), "Quantity not avaiable");
			} else {
				Order order = new Order();
				order.setEmpId(request.getUserId());
				order.setItemId(itemId);
				order.setDate(LocalDateTime.now());
				orderRepository.save(order);
				itemFromDb.setQuanitity(itemFromDb.getQuanitity()-1);
				itemRepository.save(itemFromDb);
				responseMap.put(itemFromDb.getName(), "Order success");
			}
		}
		return ResponseEntity.status(201).body(responseMap);
	}

}
