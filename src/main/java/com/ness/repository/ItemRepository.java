package com.ness.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ness.entity.Item;

public interface ItemRepository extends PagingAndSortingRepository<Item, Integer> {

	
	
	public List<Item> findByFoodType(String foodType);
	
	
	public List<Item> findByIdIn(List<Integer> itemIds);
	
	
	
	
}
