package com.ness.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ness.entity.Item;
import com.ness.repository.ItemRepository;

@Service
public class ItemService {

	
	@Autowired
	ItemRepository itemRepository;
	
	public List<Item> createItems(List<Item> items)
	{
		List<Item> result = new ArrayList<>();
		for(Item it : items)
		{
			 result.add(itemRepository.save(it));
		}
		return result;
	}
	
	public List<Item> getItems()
	{
		List<Item> items = new ArrayList<>();
		Iterable<Item> iterable =  itemRepository.findAll();
		iterable.forEach(items::add);
		return items;
	}
	
	
	public List<Item> getItems(String foodType)
	{
		return itemRepository.findByFoodType(foodType);
	}
	
	public List<Item> findItems(List<Integer> items)
	{
		
		return itemRepository.findByIdIn(items);
	}
	
}
