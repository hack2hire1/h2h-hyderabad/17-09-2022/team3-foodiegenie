package com.ness;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EntityScan
//@EnableSwagger2
public class FoodiegenieApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodiegenieApplication.class, args);
	}

}
